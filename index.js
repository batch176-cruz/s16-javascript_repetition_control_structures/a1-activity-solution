let num = parseInt(prompt("Give number:"));

console.log("The number you provided is " + num);

for (let i = num; i > 0; i--) {
	if (i <= 50) {
		console.log("The current value is " + i + ". Terminating the loop.");
		break;
	}

	if (i % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (i % 5 == 0) {
		console.log(i);
	}
}

let givenString = "supercalifragilisticexpialidocious";
console.log(givenString);

let resultString;

for (let i = 0; i < givenString.length; i++) {
	if (givenString[i] == "a" || givenString[i] == "e" || givenString[i] == "i" || givenString[i] == "o" || givenString[i] == "u") {
		continue;
	}

	resultString = resultString + givenString[i];

	if (resultString == `undefined${givenString[i]}`) {
		resultString = givenString[i];
	}
}

console.log(resultString);